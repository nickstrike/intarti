﻿using System.Collections;
using System.Collections.Generic;

public class FSM
{    
    int[,] fsm;
    int state = 0;

    public FSM(int statesCount, int eventsCount)
    {

        for (int i = 0; i < statesCount; i++)
        {
            for (int a = 0; a < eventsCount; a++)
            {
                fsm[i, a] = -1;
            }
        }
    }

    public void SetRelation(int srcState, int evt, int dstState)
    {

        fsm[srcState, evt] = dstState;
    }

    public int GetState()
    {
        return state;
    }
    public void SetEvent(int evt)
    {
        if (fsm[state, evt] != -1)
        {
            state = fsm[state, evt];
        }
    }

}
