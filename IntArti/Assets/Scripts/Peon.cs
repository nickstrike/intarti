﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peon : MonoBehaviour
{
    GameObject peon;
    bool mineEmpty = false;
    FSM _fsm;
    public float speed = 2f;

    void Start()
    {
        peon = GetComponent<GameObject>();
        _fsm = new FSM(5, 5);

        /*_fsm.SetRelation(0, 0, 1);
        _fsm.SetRelation(1, 1, 2);
        _fsm.SetRelation(2, 2, 3);
        _fsm.SetRelation(2, 4, 3);
        _fsm.SetRelation(3, 1, 4);
        _fsm.SetRelation(4, 3, 1);
        _fsm.SetRelation(4, 4, 0);*/

        _fsm.SetRelation(0, 0, 1);
        _fsm.SetRelation(1, 1, 2);
        _fsm.SetRelation(2, 2, 3);
        _fsm.SetRelation(2, 4, 4);
        _fsm.SetRelation(3, 1, 5);
        _fsm.SetRelation(4, 3, 6);
        _fsm.SetRelation(4, 4, 7);

        //_fsm.SetEvent

    }

    void Update()
    {

        switch (_fsm.GetState())
        {          //estado,condicion - array - estado destino
            case 1://idle,recolectar - 0,0 - 2
                
                break;

            case 2://Ir a mina,llegar - 1,1 - 3

                break;

            case 3://Minar,lleno - 2,2 - 5

                break;

            case 4://Minar,no oro - 2,4 - 5

                break;

            case 5://Llevar,llegar- 3,1 - 

                break;

            case 6://Depositar,vacio - 4,3 - 1

                break;

            case 7://Depositar,no oro 4,4 - 0

                break;

            default:
                Debug.Log("Error, fuera de los posibles estados");
                break;
        }


    }
}
